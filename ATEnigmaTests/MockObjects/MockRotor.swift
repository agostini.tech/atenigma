//
//  MockRotor.swift
//  ATEnigmaTests
//
//  Created by Dejan on 20/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation
@testable import ATEnigma

class MockRotor: Remappable {
    
    var shiftCalled = false
    var resetCalled = false
    var lastRemap = -1
    var lastShiftBy: [Int] = []
    
    var nextMapper: Remappable?
    
    func remap(_ x: Int) -> Int {
        self.lastRemap = x
        return x
    }
    
    func shift() {
        self.shiftCalled = true
    }
    
    func shift(by: [Int]) {
        self.lastShiftBy = by
    }
    
    func reset() {
        self.resetCalled = true
    }
}
