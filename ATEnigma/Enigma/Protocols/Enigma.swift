//
//  Enigma.swift
//  ATEnigma
//
//  Created by Dejan on 21/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Enigma {
    func encode(_ message: String) -> String
    func decode(_ message: String) -> String
}
