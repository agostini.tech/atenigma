//
//  SpindleFactory.swift
//  ATEnigma
//
//  Created by Dejan on 21/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol SpindleFactory {
    var charMappings: [Mapping<Int, Character>] { get }
    func createSpindle(withConfig config: SpindleConfiguration) -> Remappable
}
