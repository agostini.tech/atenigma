//
//  ReflectorTests.swift
//  ATEnigmaTests
//
//  Created by Dejan on 18/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import XCTest
@testable import ATEnigma

class ReflectorTests: XCTestCase {
    
    func testMappings() {
        let mappings = [Mapping(from: 1, to: 4), Mapping(from: 2, to: 7), Mapping(from: 3, to: 10), Mapping(from: 5, to: 9), Mapping(from: 6, to: 8)]
        let reflector = Reflector(withMappings: mappings)
        
        XCTAssertEqual(reflector.remap(1), 4)
        XCTAssertEqual(reflector.remap(4), 1)
        XCTAssertEqual(reflector.remap(2), 7)
        XCTAssertEqual(reflector.remap(7), 2)
        XCTAssertEqual(reflector.remap(3), 10)
        XCTAssertEqual(reflector.remap(10), 3)
        XCTAssertEqual(reflector.remap(5), 9)
        XCTAssertEqual(reflector.remap(9), 5)
        XCTAssertEqual(reflector.remap(6), 8)
        XCTAssertEqual(reflector.remap(8), 6)
        XCTAssertEqual(reflector.remap(0), -1)
        XCTAssertEqual(reflector.remap(11), -1)
    }
}
