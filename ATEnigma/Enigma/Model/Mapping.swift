//
//  Mapping.swift
//  ATEnigma
//
//  Created by Dejan on 18/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

struct Mapping<A, B> {
    let from: A
    let to: B
}
