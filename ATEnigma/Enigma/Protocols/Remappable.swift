//
//  Remappable.swift
//  ATEnigma
//
//  Created by Dejan on 18/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

protocol Remappable {
    var nextMapper: Remappable? { get }
    
    func remap(_ x: Int) -> Int
    func shift()
    func shift(by: [Int])
    func reset()
}
