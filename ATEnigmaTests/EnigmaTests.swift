//
//  EnigmaTests.swift
//  ATEnigmaTests
//
//  Created by Dejan on 21/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import XCTest
@testable import ATEnigma

class EnigmaTests: XCTestCase {
    
    func testEncoding() {
        let config = SpindleConfiguration(plugboardMappings: [Mapping(from: 23, to: 8), Mapping(from: 1, to: 10), Mapping(from: 15, to: 16), Mapping(from: 3, to: 13), Mapping(from: 5, to: 20), Mapping(from: 0, to: 21)],
                                          rotors: [2, 1, 5],
                                          rotorSettings: [11, 5, 19])
        
        let encodingEnigma = EnigmaMachine(config)
        let decodingEnigma = EnigmaMachine(config)
        
        let encodedString = encodingEnigma.encode("Hello World.")
        print("Encoded string: ", encodedString)
        let decodedString = decodingEnigma.decode(encodedString)
        
        XCTAssertEqual(decodedString, "HELLO WORLD.")
    }
    
    func testEncodingLong() {
        let message = "LOREM IPSUM DOLOR SIT AMET CONSECTETUR ADIPISCING ELIT SED DO EIUSMOD TEMPOR INCIDIDUNT UT LABORE ET DOLORE MAGNA ALIQUA. UT ENIM AD MINIM VENIAM QUIS NOSTRUD EXERCITATION ULLAMCO LABORIS NISI UT ALIQUIP EX EA COMMODO CONSEQUAT. DUIS AUTE IRURE DOLOR IN REPREHENDERIT IN VOLUPTATE VELIT ESSE CILLUM DOLORE EU FUGIAT NULLA PARIATUR. EXCEPTEUR SINT OCCAECAT CUPIDATAT NON PROIDENT SUNT IN CULPA QUI OFFICIA DESERUNT MOLLIT ANIM ID EST LABORUM."
        
        let config = SpindleConfiguration(plugboardMappings: [Mapping(from: 23, to: 8), Mapping(from: 1, to: 10), Mapping(from: 15, to: 16), Mapping(from: 3, to: 13), Mapping(from: 5, to: 20), Mapping(from: 0, to: 21)],
                                          rotors: [2, 1, 5],
                                          rotorSettings: [11, 5, 19])
        
        let encodingEnigma = EnigmaMachine(config)
        let decodingEnigma = EnigmaMachine(config)
        
        let encodedString = encodingEnigma.encode(message)
        print("Encoded: ", encodedString)
        let decodedString = decodingEnigma.decode(encodedString)
        
        XCTAssertEqual(decodedString, message)
    }
}
