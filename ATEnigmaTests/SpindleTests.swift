//
//  SpindleTests.swift
//  ATEnigmaTests
//
//  Created by Dejan on 21/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import XCTest
@testable import ATEnigma

class SpindleTests: XCTestCase {
    
    func testSpindleEncodingDecodingPair() {
        let config = SpindleConfiguration(plugboardMappings: [Mapping(from: 23, to: 8), Mapping(from: 1, to: 10), Mapping(from: 15, to: 16), Mapping(from: 3, to: 13), Mapping(from: 5, to: 20), Mapping(from: 0, to: 21)],
                                          rotors: [4, 1, 5],
                                          rotorSettings: [21, 4, 8])
        
        let spindleFactory = SmallAlphabetSpindleFactory()
        
        let encodingSpindle = spindleFactory.createSpindle(withConfig: config)
        let decodingSpindle = spindleFactory.createSpindle(withConfig: config)
        
        for i in 0...27 {
            let encoded = encodingSpindle.remap(i)
            let decoded = decodingSpindle.remap(encoded)
            XCTAssertEqual(decoded, i)
        }
    }
}
