//
//  PlugboardTests.swift
//  ATEnigmaTests
//
//  Created by Dejan on 20/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import XCTest
@testable import ATEnigma

class PlugboardTests: XCTestCase {
    
    func testPlugboardMappings() {
        let mappings = [Mapping(from: 2, to: 4), Mapping(from: 0, to: 1)]
        let mockRotor = MockRotor()
        let plugboard = Plugboard(mappings, nextMapper: mockRotor)
        
        XCTAssertFalse(mockRotor.shiftCalled)
        _ = plugboard.remap(0)
        XCTAssertEqual(mockRotor.lastRemap, 1)
        XCTAssertTrue(mockRotor.shiftCalled)
        
        _ = plugboard.remap(1)
        XCTAssertEqual(mockRotor.lastRemap, 0)
        _ = plugboard.remap(2)
        XCTAssertEqual(mockRotor.lastRemap, 4)
        _ = plugboard.remap(3)
        XCTAssertEqual(mockRotor.lastRemap, 3)
        _ = plugboard.remap(4)
        XCTAssertEqual(mockRotor.lastRemap, 2)
        _ = plugboard.remap(5)
        XCTAssertEqual(mockRotor.lastRemap, 5)
    }
}
