//
//  RotorTests.swift
//  ATEnigmaTests
//
//  Created by Dejan on 19/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import XCTest
@testable import ATEnigma

class RotorTests: XCTestCase {
    
    func testRotorMappingsWithShifting() {
        let reflectorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 5), Mapping(from: 2, to: 4)]
        let reflector: Remappable = Reflector(withMappings: reflectorMappings)
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: reflector)
        
        XCTAssertEqual(rotor.remap(0), 4)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 0)
        XCTAssertEqual(rotor.remap(5), 3)
        
        // Shift 1
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 2)
        XCTAssertEqual(rotor.remap(1), 4)
        XCTAssertEqual(rotor.remap(2), 0)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 1)
        XCTAssertEqual(rotor.remap(5), 3)
        
        // Shift 2
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 3)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 0)
        XCTAssertEqual(rotor.remap(4), 5)
        XCTAssertEqual(rotor.remap(5), 4)
        
        // Shift 3
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 2)
        XCTAssertEqual(rotor.remap(1), 3)
        XCTAssertEqual(rotor.remap(2), 0)
        XCTAssertEqual(rotor.remap(3), 1)
        XCTAssertEqual(rotor.remap(4), 5)
        XCTAssertEqual(rotor.remap(5), 4)
        
        // Shift 4
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 2)
        XCTAssertEqual(rotor.remap(1), 4)
        XCTAssertEqual(rotor.remap(2), 0)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 1)
        XCTAssertEqual(rotor.remap(5), 3)
        
        // Shift 5
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 3)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 0)
        XCTAssertEqual(rotor.remap(4), 5)
        XCTAssertEqual(rotor.remap(5), 4)
        
        // Shift 6
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 4)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 0)
        XCTAssertEqual(rotor.remap(5), 3)
    }
    
    func testNextRotorShifting() {
        let mockRotor = MockRotor()
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: mockRotor)
        
        XCTAssertFalse(mockRotor.shiftCalled)
        
        for _ in 0...5 {
            rotor.shift()
        }
        
        XCTAssertTrue(mockRotor.shiftCalled)
    }
    
    func testShiftBy() {
        let reflectorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 5), Mapping(from: 2, to: 4)]
        let reflector: Remappable = Reflector(withMappings: reflectorMappings)
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: reflector)
        
        rotor.shift(by: [3])
        
        XCTAssertEqual(rotor.remap(0), 2)
        XCTAssertEqual(rotor.remap(1), 3)
        XCTAssertEqual(rotor.remap(2), 0)
        XCTAssertEqual(rotor.remap(3), 1)
        XCTAssertEqual(rotor.remap(4), 5)
        XCTAssertEqual(rotor.remap(5), 4)
    }
    
    func testNextRotorShiftBy() {
        let mockRotor = MockRotor()
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: mockRotor)
        
        XCTAssertEqual(mockRotor.lastShiftBy, [])
        
        rotor.shift(by: [7, 4, 2])
        
        XCTAssertEqual(mockRotor.lastShiftBy, [4, 2])
    }
    
    func testReset() {
        let reflectorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 5), Mapping(from: 2, to: 4)]
        let reflector: Remappable = Reflector(withMappings: reflectorMappings)
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: reflector)
        
        XCTAssertEqual(rotor.remap(0), 4)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 0)
        XCTAssertEqual(rotor.remap(5), 3)
        
        rotor.shift()
        
        XCTAssertEqual(rotor.remap(0), 2)
        XCTAssertEqual(rotor.remap(1), 4)
        XCTAssertEqual(rotor.remap(2), 0)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 1)
        XCTAssertEqual(rotor.remap(5), 3)
        
        rotor.reset()
        
        XCTAssertEqual(rotor.remap(0), 4)
        XCTAssertEqual(rotor.remap(1), 2)
        XCTAssertEqual(rotor.remap(2), 1)
        XCTAssertEqual(rotor.remap(3), 5)
        XCTAssertEqual(rotor.remap(4), 0)
        XCTAssertEqual(rotor.remap(5), 3)
    }
    
    func testNextRotorReset() {
        let mockRotor = MockRotor()
        
        let rotorMappings = [Mapping(from: 0, to: 3), Mapping(from: 1, to: 2), Mapping(from: 2, to: 4), Mapping(from: 3, to: 1), Mapping(from: 4, to: 0), Mapping(from: 5, to: 5)]
        let rotor: Remappable = Rotor(rotorMappings, nextMapper: mockRotor)
        
        XCTAssertFalse(mockRotor.resetCalled)
        
        rotor.reset()
        
        XCTAssertTrue(mockRotor.resetCalled)
    }
}
