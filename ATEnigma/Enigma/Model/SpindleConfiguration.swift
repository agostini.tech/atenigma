//
//  SpindleConfiguration.swift
//  ATEnigma
//
//  Created by Dejan on 21/09/2018.
//  Copyright © 2018 agostini.tech. All rights reserved.
//

import Foundation

struct SpindleConfiguration {
    let plugboardMappings: [Mapping<Int, Int>]
    let rotors: [Int]
    let rotorSettings: [Int]
}
